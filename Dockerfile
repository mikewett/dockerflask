FROM python:3.5-alpine3.8
# TODO Set metadata for who maintains this image
LABEL maintainer="mikewett@hotmail.com"

COPY * /app/
RUN pip3 install Flask
RUN apk add curl
EXPOSE 8080
#TODO Set default values for env variables
ENV DISPLAY_FONT="arial"
ENV DISPLAY_COLOR="red"
ENV ENVIRONMENT="DEV"

#TODO *bonus* add a health check that tells docker the app is running properly
HEALTHCHECK CMD curl --fail http://localhost:8080 || exit 1

# TODO have the app run as a non-root user

RUN adduser -S mikewett
USER mikewett

CMD python /app/app.py
